import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(private rutas: Router) { }

  ngOnInit() {
  }

  login() {
    this.rutas.navigateByUrl("/login");
  }

  registro() {
    this.rutas.navigateByUrl("/registro");
  }

}
