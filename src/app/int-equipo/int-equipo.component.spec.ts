import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntEquipoComponent } from './int-equipo.component';

describe('IntEquipoComponent', () => {
  let component: IntEquipoComponent;
  let fixture: ComponentFixture<IntEquipoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntEquipoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntEquipoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
