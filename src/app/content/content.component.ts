import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { AsideComponent } from "app/aside/aside.component";
import { EquiposComponent } from "app/equipos/equipos.component";
import { IntEquipoComponent } from "app/int-equipo/int-equipo.component";

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
  
  @Input()
  viewing: any;
  
  @Output() changeContent: EventEmitter<any> = new EventEmitter();
  @Output() createGrupo: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
  /*
  getNotification(data) {
    console.log("getting a notification: "+data);
  }
  */

  getChangeTeam(data){
    this.changeContent.emit(data);
    console.log("Llega esto desde mi hermano getChangeTeam(): "+data);
  }

  getCreateGroup(data){
    this.createGrupo.emit(data);
    console.log("Llega esto desde mi hermano getCreateGroup(): "+data);
  }
  
}
