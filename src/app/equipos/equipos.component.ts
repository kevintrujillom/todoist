import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-equipos',
  templateUrl: './equipos.component.html',
  styleUrls: ['./equipos.component.scss']
})
export class EquiposComponent implements OnInit {

  @Output() changeTeam: EventEmitter<any> = new EventEmitter();
  @Output() createGroup: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  loadTeam(component: String) {
    this.changeTeam.emit(component);
    console.log("valor de loadTeam: " + component);
  }

  crearGrupo(component: String) {
    this.createGroup.emit(component);
    console.log("valor de createGroup: " + component);
  }
}
