import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-escritorio',
  templateUrl: './escritorio.component.html',
  styleUrls: ['./escritorio.component.scss']
})
export class EscritorioComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  viewing: String = "home";

  getChange(newContentView) {
    this.viewing = newContentView;
    console.log("viewing getChange: "+this.viewing);
  }
  
}
