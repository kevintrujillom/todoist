import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { AsideComponent } from './aside/aside.component';
import { ContentComponent } from './content/content.component';
import { HomeComponent } from './home/home.component';
import { EquiposComponent } from './equipos/equipos.component';
import { TareasComponent } from './tareas/tareas.component';
import { LoginComponent } from './login/login.component';
import { IntEquipoComponent } from './int-equipo/int-equipo.component';
import { RegistroComponent } from './registro/registro.component';
import { MenuComponent } from './menu/menu.component';
import { EscritorioComponent } from './escritorio/escritorio.component';
import { RouterModule,Routes} from '@angular/router';
import { CreargrupoComponent } from './creargrupo/creargrupo.component';

const rutas: Routes = [
  { path: 'registro', component: RegistroComponent },
  { path: 'login', component: LoginComponent },
  { path: 'escritorio', component: EscritorioComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AsideComponent,
    ContentComponent,
    HomeComponent,
    EquiposComponent,
    TareasComponent,
    LoginComponent,
    IntEquipoComponent,
    RegistroComponent,
    MenuComponent,
    EscritorioComponent,
    CreargrupoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(rutas),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
